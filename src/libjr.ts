import * as dateutils from "./dateutils";

function getClass(obj: any)
{
	return obj.constructor;
}

export interface TranslatedString
{
	cz?: string;
	en?: string;
	de?: string;
	sk?: string;
}

export interface TransportClass
{
	id: any;
	name: TranslatedString;
}

export interface Place
{
	name: string;
}

export interface Route
{
	distance: number; //in km
	length: dateutils.TimeDelta;
	segments: RouteSegment[];
}

export interface RouteSegment
{
	departureDate: Date;
	arrivalDate: Date;
	distance: number; //in km
	length: dateutils.TimeDelta;
	designation: string;
	places: RoutePlace[];
}

export interface RoutePlace extends Place
{
	name: string;
	arrivalDate: Date;
	departureDate: Date;

}

export interface RouteBackend
{
	transportClasses: TransportClass[];
}

export interface PlaceListBackend
{
	findPlaces(tcl: TransportClass, searchStr: string,
		onComplete: (results: SearchResults<Place>) => void):
		SearchResults<Place> | void
}

export interface RouteListBackend
{
	findRoutes(tcl: TransportClass, fromPlace: Place, toPlace: Place):
		SearchResults<Route>;
}

export interface SearchResults<T> extends Array<T>
{
	[index: number]: T;
	getPrevious(): T[];
	getNext(): T[];
	//getPrevious and getNext should return the items added,
	//[] when nothing added
}

export interface SearchResultsReqFunc<T>
{
	(srs: SearchResults<T>): T[];
}

export class JRError extends Error
{
	static errName = "";
	constructor(message: string)
	{
		super();

		this.name = getClass(this).errName;
		this.message = message;
	}
}

export class PlaceNotFoundError extends JRError
{
	static errName = "PlaceNotFoundError";
	place: Place;
	constructor(place, message = "Place not found")
	{
		super(message);
		this.place = place;
	}
}

export class NoResultsError extends JRError
{
	static errName = "NoResultsError";
}

export class CRWSSearchResults<T> extends Array implements SearchResults<T>
{
	handle: any;
	results: any[];
	getPreviousF: SearchResultsReqFunc<T>;
	getNextF: SearchResultsReqFunc<T>;

	constructor(handle: any, results: any[],
		getNext?: SearchResultsReqFunc<T>,
		getPrevious?: SearchResultsReqFunc<T>)
	{
		super();
		this.push.apply(this, results);

		this.handle = handle;
		this.results = results;
		this.getNextF = getNext;
		this.getPreviousF = getPrevious;
	}

	getPrevious(): T[]
	{
		if(this.getPreviousF)
		{
			return this.getPreviousF(this);
		}
		else
		{
			return [];
		}
	}

	getNext(): T[]
	{
		if(this.getNextF)
		{
			return this.getNextF(this);
		}
		else
		{
			return [];
		}
	}
}

export interface CRWSRoute extends Route
{
	id: number;
}

export class CRWSBackend implements RouteBackend, PlaceListBackend,
	RouteListBackend
{
	static serverProxyURL = "/cgi-bin/api.py";
	static serverURL = "/";
	static dateTimeFormat = "%d.%m.%Y %H:%M";

	userID: string = "D4E34283-15DF-4AD7-9EA4-9AA51BAB4A98";
	transportClasses: TransportClass[];

	sendRequest(reqname: string, args: Object,
		onComplete?: (result: any) => void): any
	{
		var url = getClass(this).serverURL + reqname;
		args["userID"] = this.userID;

		var first = true;
		for(var arg in args)
		{
			if(first)
			{
				url += `?${arg}=${encodeURIComponent(args[arg])}`;
				first = false;
			}
			else
			{
				url += `&${arg}=${encodeURIComponent(args[arg])}`;
			}
		}
		
		var xhr = new XMLHttpRequest();
		if(onComplete)
		{
			xhr.open("POST", getClass(this).serverProxyURL, true);
			xhr.addEventListener("load", () =>
			{
				if(xhr.responseText == "")
				{
					throw new NoResultsError("Empty response");
				}
				onComplete(JSON.parse(xhr.responseText));
			})
			xhr.send(url);
		}
		else
		{
			xhr.open("POST", getClass(this).serverProxyURL, false);
			xhr.send(url);
			if(xhr.responseText == "")
			{
				throw new NoResultsError("Empty response");
			}
			return JSON.parse(xhr.responseText);
		}
	}
	
	static dataToTranslatedString(resp: string[])
	{
		return {
			cz: resp[0],
			en: resp[1],
			de: resp[2],
			sk: resp[3]
		};
	}

	constructor()
	{
		this.transportClasses = this.getTransportClasses();
	}

	getTransportClasses(): TransportClass[]
	{
		var resp = this.sendRequest("", {});
		var tcs: TransportClass[] = [];
		for(var i = 0; i < resp.data.length; i++)
		{
			tcs.push({
				id: resp.data[i].id,
				name: CRWSBackend.dataToTranslatedString(resp.data[i].name)
			});
		}

		return tcs;
	}
	
	static dataToPlaces(data: any): Place[]
	{
		var stations: Place[] = [];
		for(var i = 0; i < data.length; i++)
		{
			stations.push({
				name: data[i].name/*,
				lon: data[i].coorY,
				lat: data[i].coorX*/});
			//TODO: lon/lat combination may not be correct
			//TODO: lon/lat don't get sent for some silly reason...
		}
		return stations;
	}

	findPlaces(tcl: TransportClass, searchStr: string,
		onComplete: (results: SearchResults<Place>) => void):
		SearchResults<Place>
	{
		if(onComplete)
		{
			this.sendRequest(tcl.id + "/globalListItems/",
				{"mask": searchStr}, (resp) =>
			{
				if(!resp.data)
				{
					//No results found
					return null;
				}
				onComplete(new CRWSSearchResults<Place>({searchStr: searchStr,
					tcl: tcl}, CRWSBackend.dataToPlaces(resp.data),
					(srs: any): any => {return this.getNextPlaces(srs);}));
			});
		}
		else
		{
			var resp = this.sendRequest(tcl.id + "/globalListItems/",
				{"mask": searchStr});
			if(!resp.data)
			{
				//No results found
				return null;
			}
			
			return new CRWSSearchResults<Place>({searchStr: searchStr,
				tcl: tcl}, CRWSBackend.dataToPlaces(resp.data),
				(srs: any): any => {return this.getNextPlaces(srs);});
				//That lambda is "this" trickery
		}
	}

	getNextPlaces(srs: CRWSSearchResults<Place>): Place[]
	{
		var resp = this.sendRequest(srs.handle.tcl.id + "/globalListItems/",
			{"mask": srs.handle.searchStr, "skipCount": srs.length});
		var places = CRWSBackend.dataToPlaces(resp.data);
		srs.push.apply(srs, places);
		return places;
	}
	
	static dataToTimeDelta(data: string): dateutils.TimeDelta
	{
		return new dateutils.TimeDelta(0,
			parseInt((data.match(/([0-9]+) min/) || ["0", "0"])[1]),
			parseInt((data.match(/([0-9]+) hod/) || ["0", "0"])[1]));
	}

	static dataToDate(data: string): Date
	{
		return dateutils.parseDate(data, CRWSBackend.dateTimeFormat);
	}

	static dataToAddedTime(date: Date, data: string): Date
	{
		//Converts a time string (%H:%M) to a date by setting the hours and
		//minutes of date to its value
		var resdate = new Date(date.getTime());
		var colonSplit = data.split(":");
		var mins = parseInt(colonSplit[1]);
		var hours = parseInt(colonSplit[0]);
		resdate.setMinutes(mins);
		resdate.setHours(hours);
		resdate.setSeconds(0); //Just in case
		return resdate
	}
	
	static dataToRoutePlaces(data: any, departureDate: Date): RoutePlace[]
	{
		var places = [];
		for(var i = 0; i < data.length; i++)
		{
			var placeData = data[i];
			places.push({
				name: placeData.station.name,
				arrivalDate: placeData.arrTime ?
					CRWSBackend.dataToAddedTime(departureDate,
					placeData.arrTime) : null,
				departureDate: placeData.depTime ?
					CRWSBackend.dataToAddedTime(departureDate,
					placeData.depTime) : null
			});
			//XXX: There's no good way to deal with travels
			//spanning several days, so just don't use the "date"
			//portion of the Date object
		}

		return places;
	}

	static dataToSegments(data: any): RouteSegment[]
	{
		var segments = [];
		for(var i = 0; i < data.length; i++)
		{
			var segmentData = data[i];
			var segment: any = {
				departureDate: CRWSBackend.dataToDate(segmentData.dateTime1),
				arrivalDate: CRWSBackend.dataToDate(segmentData.dateTime2),
				distance: segmentData.distance ?
					parseInt(segmentData.distance.slice(0, -3)) : 0,
				length: CRWSBackend.dataToTimeDelta(segmentData.timeLength),
				designation: `${segmentData.trainData.info.type} ` +
					`${segmentData.trainData.info.num1}` +
					(segmentData.trainData.info.num2 ?
					` ${segmentData.trainData.info.num2}` : "")
			};
			segment.places = CRWSBackend.dataToRoutePlaces(segmentData
				.trainData.route, segment.departureDate);
			segments.push(segment);
		}

		return segments;
	}

	static dataToRoutes(data: any): CRWSRoute[]
	{
		var routes = [];
		var routeData, route;
		for(var i = 0; i < data.length; i++)
		{
			routeData = data[i];
			route = {
				distance: routeData.distance ?
					parseInt(routeData.distance.slice(0, -3)) : 0,
				length: CRWSBackend.dataToTimeDelta(routeData.timeLength),
				segments: CRWSBackend.dataToSegments(routeData.trains),
				id: routeData.id
			};
			routes.push(route);
		}

		return routes;
	}


	findRoutes(tcl: TransportClass, fromPlace: Place, toPlace: Place,
		date?: Date, dateOfArrival = false)
	{
		//if dateOfArrival == true, date is treated as an expected
		//date of arrival, otherwise it is treated as an expected
		//date of departure.
		var params = {"from": fromPlace.name, "to": toPlace.name};
		if(date)
		{
			params["dateTime"] = dateutils.formatDate(date,
				CRWSBackend.dateTimeFormat);
			params["isDep"] = !dateOfArrival;
		}
		var resp = this.sendRequest(tcl.id + "/connections", params);
		if(!resp.fromObjects[0].timetableObject)
		{
			throw new PlaceNotFoundError(fromPlace);
		}
		if(!resp.toObjects[0].timetableObject)
		{
			throw new PlaceNotFoundError(toPlace);
		}
		if(!resp.connInfo)
		{
			throw new NoResultsError("No routes found");
		}
		return new CRWSSearchResults<CRWSRoute>(
			{combID: resp.combId, handle: resp.handle},
			CRWSBackend.dataToRoutes(resp.connInfo.connections),
			(srs: any) => {return this.getNextRoutes(srs);},
			(srs: any) => {return this.getPreviousRoutes(srs);});
	}

	getNextRoutes(srs: CRWSSearchResults<CRWSRoute>): CRWSRoute[]
	{
		var resp = this.sendRequest(srs.handle.combID + "/connections/" +
			srs.handle.handle, {connId: srs[srs.length - 1].id});
		var routes = CRWSBackend.dataToRoutes(resp.connections);
		srs.push.apply(srs, routes);
		return routes;
	}

	getPreviousRoutes(srs: CRWSSearchResults<CRWSRoute>): CRWSRoute[]
	{
		var resp = this.sendRequest(srs.handle.combID + "/connections/" +
			srs.handle.handle, {connId: srs[0].id, prevConn: true});
		var routes = CRWSBackend.dataToRoutes(resp.connections);
		srs.unshift.apply(srs, routes);
		return routes;
	}
}

/*
export class CRWSSOAPBackend implements RouteBackend, PlaceListBackend,
	RouteListBackend
{
	static serverProxyURL = "/cgi-bin/api_soap.py";
	userID = "F774E105-66A6-4F48-BB5D-58622D75F4F9";
	transportClasses: TransportClass[];
}
*/
