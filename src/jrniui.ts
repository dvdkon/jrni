import Awesomplete = require("awesomplete");
import * as tswgt from "./tswgt";
import * as libjr from "./libjr";
import * as dateutils from "./dateutils";
var t = tswgt.translate; //Convenience shortcut

var jrbackend = new libjr.CRWSBackend();

var settings: any = {
};

var icons = {
	menu: "images/menu.svg",
	about: "images/about.svg",
	search: "images/search.svg"
}

var dateFormat = "%d.%m.%Y";
var timeFormat = "%H:%M";
var dateTimeFormat = dateFormat + " " + timeFormat;

function tclFromID(id: any): libjr.TransportClass
{
	for(var i = 0; i < jrbackend.transportClasses.length; i++)
	{
		var tcl = jrbackend.transportClasses[i];
		if(tcl.id == id)
		{
			return tcl;
		}
	}

	return null;
}

class SideMenuButton extends tswgt.Widget
{
	static cssClass = "SideMenuButton";
	static css: string = `.SideMenuButton
{
	margin: 10px;
	height: 30px;
}

.SideMenuButton img
{
	float: left;
	height: 30px;
}

.SideMenuButton .label
{
	float: right;
	line-height: 30px;
	vertical-align: middle;
	font-size: 20px;
	color: white;
}`;
	static html: string = `<img src="{{= it.iconURL }}"/><span class="label">
{{= it.text }}</span>`;
	constructor(iconURL: string, text: string)
	{
		super({iconURL: iconURL, text: text});
	}
}

class SideMenu extends tswgt.VList
{
	static cssClass = "SideMenu";
	static css: string = `.SideMenu
{
	position: fixed;
	top: 0;
	bottom: 0;
	left: 0;
	background-color: #180f77;
	visibility: hidden;
	margin-top: 40px; /*To not collide with the top bar*/
	width: 200px;
	z-index: 2;
}`;
	
	shown: boolean = false;
	aboutButton: SideMenuButton;
	searchRouteButton: SideMenuButton;
	constructor()
	{
		super();
	}

	onDOMCreated(): void
	{
		this.aboutButton = new SideMenuButton(icons.about,
			t("sideMenu.about"));
		this.aboutButton.addEventListener("click", function()
		{
			new AboutView().show();
			sideMenu.hide();
		});
		this.addWidget(this.aboutButton);

		this.searchRouteButton = new SideMenuButton(icons.search,
			t("sideMenu.searchRoute"));
		this.searchRouteButton.addEventListener("click", function()
		{
			new SearchRouteView().show();
			sideMenu.hide();
		});
		this.addWidget(this.searchRouteButton);
	}

	hide(): void
	{
		this.shown = false;
		this.setStyle({"visibility": "hidden"});
	}

	show(): void
	{
		this.shown = true;
		this.setStyle({"visibility": "visible"})
	}
	toggle(): void
	{
		if(this.shown)
		{
			this.hide()
		}
		else
		{
			this.show()
		}
	}
}
var sideMenu: SideMenu; //SideMenu should only have this instance in this app

class TopBar extends tswgt.HList
{
	static cssClass = "TopBar";
	static css: string = `.TopBar
{
	background-color: #0f3b77;
	height: 40px;
	position: fixed;
	top: 0px;
	width: 100%;
	z-index: 2;
}

.TopBar *
{
	height: 100%;
}`;
	
	sideMenuButton: tswgt.ImageWidget;
	viewButtons: tswgt.Widget[] = [];

	constructor()
	{
		super();
	}
	
	onDOMCreated(): void
	{
		//Common elements
		this.sideMenuButton = new tswgt.ImageWidget(icons.menu);
		this.sideMenuButton.addEventListener("click", function()
		{
			sideMenu.toggle();
		});
		this.addWidget(this.sideMenuButton);
	}

	addViewButton(wgt: tswgt.Widget): void
	{
		this.viewButtons.push(wgt);
		this.addWidget(wgt);
	}

	removeViewButton(wgt: tswgt.Widget): void
	{
		var index = this.viewButtons.indexOf(wgt);
		this.removeWidget(wgt);
		this.viewButtons.splice(index, 1);
	}
}
var topBar: TopBar;

class TextInput extends tswgt.Input
{
	static cssClass = "TextInput";
	static css: string = `.TextInput input
{
	width: 100%;
	height: 30px;
	border-style: solid;
	border-width: 1px;
	border-color: #333333;
	border-radius: 2px;
	background-color: #FAFAFA;
}`;
	constructor()
	{
		super("text");
	}
}

class AwesompleteTextInput extends TextInput
{
	static cssClass = "";
	awesomplete: Awesomplete;
	constructor(values: string[])
	{
		super();
		this.addOnDOMCreatedListener(() =>
		{
			this.awesomplete = new Awesomplete(
				<HTMLInputElement> this.domElement.getElementsByTagName(
				"input")[0], {
				list: values
			});
		});
	}
}

class AJAXAwesompleteTextInput extends AwesompleteTextInput
{
	static cssClass = "";
	//List of values which have been at some point entered into the textbox
	//from oldest to newest. Values get removed when an AJAX request for them
	//or a newer value completes
	inputs: string[] = [];

	i = 0;

	constructor(values: string[], fetchValues: (input: string) => void)
	{
		super(values);
		this.addOnDOMCreatedListener(() =>
		{
			this.awesomplete.input.addEventListener("input", () =>
			{
				this.inputs.push(this.awesomplete.input.value);
				fetchValues(this.awesomplete.input.value);
			});
		});
		//setInterval(() => {console.log(this.i, this.awesomplete.selected); this.i++;}, 1000);
	}

	onValuesReceived(input: string, values: string[])
	{
		var index = this.inputs.indexOf(input);
		if(index == -1) //A newer set of values has been received already
		{
			return;
		}

		this.awesomplete.list = values;
		if(this.awesomplete.opened && !this.awesomplete.selected)
		{
			this.awesomplete.evaluate();
		}

		this.inputs = this.inputs.slice(index + 1);
	}

}

class PlaceAwesompleteTextInput extends AJAXAwesompleteTextInput
{
	static cssClass = "";
	tcl: libjr.TransportClass;

	constructor(tcl: libjr.TransportClass)
	{
		//TODO: have commonly used values be defaults
		super([], (input) =>
		{
			try
			{
				jrbackend.findPlaces(this.tcl, input, (places) =>
				{
					this.onValuesReceived(input, places.map(
						(val) => {return val.name}));
				});
			}
			catch(e)
			{
				if(!(e instanceof libjr.NoResultsError))
				{
					throw e;
				}
			}
		});

		this.tcl = tcl;
	}
}

class View extends tswgt.Widget
{
	static cssClass = "View";
	static css: string = `.View
	{
		margin-top: 40px;
	}`;
	
	show(): void
	{
		topBar.viewButtons.forEach((wgt) =>
		{
			topBar.removeViewButton(wgt);
		});
		if(currentView)
		{
			tswgt.body.removeChild(currentView);
		}
		tswgt.body.appendChild(this);
		currentView = this;
	}
}
var currentView: View;

class ButtonWidget extends tswgt.ButtonWidget
{
	static cssClass = "Button_jrniui";
	static css: string = `.Button_jrniui button
{
	height: 30px;
	border-style: solid;
	border-width: 1px;
	border-color: #333333;
	border-radius: 2px;
	background-color: #FAFAFA;
}`;
}

class SelectWidget extends tswgt.SelectWidget
{
	static cssClass = "Select_jrniui";
	static css = `.Select_jrniui select
{
	width: 100%;
	height: 30px;
	border-style: solid;
	border-width: 1px;
	border-color: #333333;
	border-radius: 2px;
	background-color: #FAFAFA;
	min-width: -moz-min-content;
	min-width: -webkit-min-content;
	/* This isn't supported well...Maybe just use fixed width? */
	min-width: min-content;
}`;
}

class SearchRouteView extends View
{
	static cssClass = "SearchRouteView";
	static css: string = `.View.SearchRouteView
{
	margin-top: 50px;
	margin-left: 10px;
	margin-right: 10px;
}

.SearchRouteView .errorLabel
{
	color: red;
	text-align: center;
}

.SearchRouteView input[type=text]
{
	box-sizing: border-box;
}

.SearchRouteView .awesomplete
{
	width: 100%;
}

.SearchRouteView > .Select_jrniui
{
	margin-bottom: 10px;
}

.SearchRouteView .inputTable
{
	display: table;
	width: 100%;
}

.SearchRouteView .inputTable > *
{
	display: table-row;
}

.SearchRouteView .inputTable > * > *
{
	display: table-cell;
	padding-bottom: 10px;
}
.SearchRouteView .inputTable > * > span
{
	vertical-align: middle;
}

.SearchRouteView .inputTable > * > .TextInput
{
	width: 100%;
	padding-left: 20px;
}`;

	static html = `
{{= it.errorLabel.getHTML() }}
{{= it.transportClassSelect.getHTML() }}
<div class="inputTable">
	<div><span>${ t("searchRouteView.from") }</span>
	{{= it.fromTextInput.getHTML() }}</div>
	<div><span>${ t("searchRouteView.to") }</span>
	{{= it.toTextInput.getHTML() }}</div>
	<div>{{= it.dateTypeSelect.getHTML() }}
	{{= it.dateInput.getHTML() }}</div>
</div>
{{= it.searchButton.getHTML() }}`;

	errorLabel: tswgt.Label;
	transportClassSelect: SelectWidget;
	fromTextInput: PlaceAwesompleteTextInput;
	toTextInput: PlaceAwesompleteTextInput;
	dateTypeSelect: SelectWidget;
	dateInput: TextInput;
	searchButton: ButtonWidget;

	constructor()
	{
		super();
		this.templateData = this;

		this.errorLabel = new tswgt.Label("");
		this.errorLabel.addOnDOMCreatedListener(() =>
		{
			this.errorLabel.domElement.classList.add("errorLabel");
		})
		this.transportClassSelect = new SelectWidget();
		this.transportClassSelect.addOnDOMCreatedListener((tcs) =>
		{
			for(var i = 0; i < jrbackend.transportClasses.length; i++)
			{
				var tc = jrbackend.transportClasses[i];
				this.transportClassSelect.addChoice(tc.id,
					tc.name[tswgt.language]);
			}
		});
		this.transportClassSelect.addEventListener("change", () =>
		{
			var tcl = tclFromID(this.transportClassSelect.getValue());
			this.fromTextInput.tcl = tcl;
			this.toTextInput.tcl = tcl;
		});
		this.fromTextInput = new PlaceAwesompleteTextInput(
			jrbackend.transportClasses[0]);
		this.toTextInput = new PlaceAwesompleteTextInput(
			jrbackend.transportClasses[0]);
		this.dateTypeSelect = new SelectWidget({
			departure: t("searchRouteView.departureDate"),
			arrival: t("searchRouteView.arrivalDate")});
		this.dateInput = new TextInput();
		this.searchButton = new ButtonWidget(t("searchRouteView.search"));
		this.searchButton.addOnDOMCreatedListener(() =>
		{
			this.searchButton.setStyle({"float": "right"});
		});
		this.searchButton.addEventListener("click", () =>
		{
			var tclassid = this.transportClassSelect.getValue();
			var tc = tclFromID(tclassid);
			if(this.fromTextInput.getValue() == "" ||
				this.toTextInput.getValue() == "")
			{
				this.setErrorLabel(t("searchRouteView.placeEmpty"));
			}

			var dateStr = this.dateInput.getValue();
			var date: Date;
			if(dateStr != "")
			{
				//It's awful, but it works (at least I hope so)
				try
				{
					date = dateutils.parseDate(dateStr, dateTimeFormat);
				}
				catch(e)
				{
					try
					{
						date = dateutils.parseDate(dateStr, timeFormat);
					}
					catch(e)
					{
						try
						{
							date = dateutils.parseDate(dateStr, dateFormat);
						}
						catch(e)
						{
							this.setErrorLabel(
								t("searchRouteView.dateFormatError"));
						}
					}
				}
			}

			try
			{
				console.log(date);
				var routes = jrbackend.findRoutes(tc,
					{name: this.fromTextInput.getValue()},
					{name: this.toTextInput.getValue()},
					date);
				new RoutesView(routes).show();
			}
			catch(e)
			{
				if(e instanceof libjr.PlaceNotFoundError)
				{
					this.setErrorLabel(t("searchRouteView.placeNotFound") +
						": " + e.place.name);
				}
				else if(e instanceof libjr.NoResultsError)
				{
					this.setErrorLabel(t("searchRouteView.noRouteFound"));
				}
				else
				{
					throw e;
				}
			}
		});
	}

	setErrorLabel(text: string): void
	{
		this.errorLabel.setText(text);
		if(text == "" || !text)
		{
			this.errorLabel.setStyle({"margin-bottom": "0"});
		}
		else
		{
			this.errorLabel.setStyle({"margin-bottom": "10px"});
		}
	}
}

class RouteWidget extends tswgt.Widget
{
	static cssClass = "RouteWidget";
	static css = `.RouteWidget
{
	display: table;
	width: 100%;
	border-bottom-style: solid;
	border-bottom-width: 1px;
	border-bottom-color: #AAA;
	padding-bottom: 5px;
	padding-top: 5px;
}

.RouteWidget .row
{
	display: table-row;
}

.RouteWidget div span
{
	display: table-cell;
	width: 100%;
}

.RouteWidget .segment
{
	margin-top: 5px;
	padding-top: 5px;
}

.RouteWidget .segment:first-child
{
	border-top-style: none;
	padding-top: 0;
}

.RouteWidget .segment .arr,.dep
{
	min-width: 50px;
}`;

	static html = `
<span class="info">{{= it.route.length.format() }}, \
{{= it.route.distance }} ${ t("routeWidget.kilometers") }</span>
{{~it.route.segments :segment}}
	<div class="segment">
		{{=segment.designation}}
		{{~segment.places :place}}
			<div class="row place">
				<span class="name">{{=place.name}}</span>
				<span class="arr">
					{{?place.arrivalDate}}
						{{= it.dateutils.formatDate(place.arrivalDate,
							it.timeFormat) }}
					{{?}}
				</span>
				<span class="dep">
					{{?place.departureDate}}
						{{= it.dateutils.formatDate(place.departureDate,
							it.timeFormat) }}
					{{?}}
				</span>
			</div>
		{{~}}
	</div>
{{~}}`;

	constructor(route: libjr.Route)
	{
		super({route: route, dateutils: dateutils, timeFormat: timeFormat});
	}
}

class RoutesView extends View
{
	static cssClass = "RoutesView";
	static css: string = `.RoutesView
{
	
}

.RoutesView .nextButton,.previousButton
{
	height: 40px;
	line-height: 40px;
	text-align: center;
	background-color: #EEE;
}

.RoutesView .nextButton
{
	box-shadow: 0px -2px 2px #AAA;
	margin-top: 10px;
}

.RoutesView .previousButton
{
	box-shadow: 0px 2px 2px #AAA;
	margin-bottom: 10px;
}

.RoutesView .routeWidgets .RouteWidget:last-child
{
	padding-bottom: 0;
	border-bottom-style: none;
}`;
	static html = `
{{= it.this.previousButton.getHTML() }}
<div class="routeWidgets">
	{{~it.routes :route}}
		{{ var rw = new it.RouteWidget(route);
			it.this.routeWidgets.push(rw); }}
		{{= rw.getHTML() }}
	{{~}}
</div>
{{= it.this.nextButton.getHTML() }}`;
	
	routeWidgets: RouteWidget[];
	nextButton: tswgt.Label;
	previousButton: tswgt.Label;
	constructor(routes: libjr.SearchResults<libjr.Route>)
	{
		super({routes: routes, RouteWidget: RouteWidget});
		this.templateData.this = this;
		this.routeWidgets = [];
		this.nextButton = new tswgt.Label(t("routesView.next"));
		this.nextButton.addOnDOMCreatedListener(() =>
		{
			this.nextButton.domElement.classList.add("nextButton");
		});
		this.nextButton.addEventListener("click", () =>
		{
			this.addRoutes(routes.getNext());
		});
		this.previousButton = new tswgt.Label(t("routesView.previous"));
		this.previousButton.addOnDOMCreatedListener(() =>
		{
			this.previousButton.domElement.classList.add("previousButton");
		});
		this.previousButton.addEventListener("click", () =>
		{
			this.addRoutes(routes.getPrevious(), true);
		})
	}

	addRoutes(routes: libjr.Route[], toFront = false)
	{
		routes.forEach((route) =>
		{
			var rw = new RouteWidget(route);
			if(toFront)
			{
				this.domElement.getElementsByClassName("routeWidgets")[0]
					.insertBefore(rw.domElement,
					this.routeWidgets[0].domElement);
			}
			else
			{
				this.domElement.getElementsByClassName("routeWidgets")[0]
					.appendChild(rw.domElement);
			}
		});
	}
}

class AboutView extends View
{
	static cssClass = "AboutView";
	static css = "";
	static html: string = `<p>${t("aboutView.aboutText")}<p>`;
}

document.addEventListener("DOMContentLoaded", function()
{
	tswgt.init();
	tswgt.body.setStyle({"margin": "0", "font-family": "sans-serif"});
	
	sideMenu = new SideMenu();
	tswgt.body.appendChild(sideMenu);

	topBar = new TopBar();
	tswgt.body.appendChild(topBar);

	new SearchRouteView().show();
});

