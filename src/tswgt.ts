import doT = require("dot");

var stylesheet: HTMLStyleElement;
export var body: Widget;
var nextWidgetID = 0;
var uninitialisedWidgets: Widget[] = [];
export function init(): void
{
	stylesheet = document.createElement("style");
	stylesheet.type = "text/css";
	document.head.appendChild(stylesheet);

	body = new Widget({}, document.body);
}

var observer = new MutationObserver((mutations) =>
{
	mutations.forEach((mutation) =>
	{
		for(var i = 0; i < mutation.addedNodes.length; i++)
		{
			var node = (<HTMLElement> mutation.addedNodes[i]);
			if(node instanceof HTMLElement)
			{
				var widgetElements = node.getElementsByClassName("Widget");
				for(var j = 0; j < widgetElements.length; j++)
				{
					var element = <HTMLElement> widgetElements[j];
					uninitialisedWidgets.forEach((widget) =>
					{
						if(element.id == widget.id.toString())
						{
							var index = uninitialisedWidgets.indexOf(widget);
							uninitialisedWidgets.splice(index, 1);
							widget._domElement = element;
							widget.callOnDOMCreatedListeners();
						}
					});
				}
			}
		}
	});
});
observer.observe(document, {
	childList: true,
	subtree: true});

export function addCSS(css: string): void
{
	stylesheet.insertAdjacentHTML("beforeend", css + "\n");
}

export function getClass(obj: any): any
{
	return obj.constructor;
}

export class Widget
{
	static cssClass = "Widget";
	static css = "";
	static cssLoaded: boolean = false;
	static html = "";
	_domElement: HTMLElement;
	id: number;
	templateData: any;
	onDOMCreatedListeners: ((widget: Widget) => void)[] = [];
	
	/* templateData: data passed to the template to render
	 * domElement: initialize from existing DOM element. Do not use with
	 * inTemplate = true
	 */
	constructor(templateData: any = {}, domElement?: HTMLElement)
	{
		this.id = nextWidgetID++;
		
		if(!getClass(this).cssLoaded)
		{
			addCSS(getClass(this).css);
			getClass(this).cssLoaded = true;
		}

		if(domElement)
		{
			this._domElement = domElement;
			return;
		}

		this.templateData = templateData;
		uninitialisedWidgets.push(this);
	}

	getClassList(): string[]
	{
		var proto: any = (<any>this).__proto__;
		var parentClass: any;
		var classList: string[] = [];
		while(proto.__proto__ != null)
		{
			parentClass = proto.constructor;
			if(!parentClass.cssLoaded)
			{
				stylesheet.insertAdjacentHTML("beforeend", parentClass.css);
				parentClass.cssLoaded = true;
			}
			if(parentClass.cssClass != "")
			{
				classList.push(parentClass.cssClass);
			}

			proto = proto.__proto__;
		}

		return classList;
	}
	
	getHTML(): string
	{
		var template = doT.template(getClass(this).html);
		return `<div class="${this.getClassList().join(" ")}" ` +
			`id="${this.id}">${template(this.templateData)}</div>`;
	}
	
	get domElement(): HTMLElement
	{
		if(!this._domElement)
		{
			var index = uninitialisedWidgets.indexOf(this);
			uninitialisedWidgets.splice(index, 1);
			var classList = this.getClassList();
			var template = doT.template(getClass(this).html);
			this._domElement = document.createElement("div");
			this._domElement.innerHTML = template(this.templateData);
			for(var i = classList.length - 1; i >= 0; i--)
			{
				this._domElement.classList.add(classList[i]);
			}

			this.callOnDOMCreatedListeners();
		}

		return this._domElement;
	}

	//style: dictionary of CSS attributes and their values
	setStyle(style: Object): void
	{
		for(var attr in style)
		{
			this.domElement.style[attr] = style[attr];
		}
	}

	/* The returned object works a bit weird - it changes the appearance of the
	 * widget as soon as its properties are changed
	 * Using one as a parameter to setStyle may also be inefficient, but who
	 * cares about a few hundred variables when we're using CSS...
	 */
	getStyle(): CSSStyleDeclaration
	{
		return this.domElement.style;
	}

	appendChild(widget: Widget): void
	{
		this.domElement.appendChild(widget.domElement);
	}

	removeChild(widget: Widget): void
	{
		this.domElement.removeChild(widget.domElement);
	}

	addEventListener(event: string, listener: Function): void
	{
		this.addOnDOMCreatedListener(() =>
		{
			this.domElement.addEventListener(event, function(e)
			{
				listener(this, e);
			});
		});
	}
	
	addOnDOMCreatedListener(listener: (obj: any) => void)
	{
		if(this._domElement)
		{
			listener(this);
		}
		else
		{
			this.onDOMCreatedListeners.push(listener);
		}
	}

	callOnDOMCreatedListeners(): void
	{
		this.onDOMCreated();
		this.onDOMCreatedListeners.forEach((listener) =>
		{
			listener(this);
		});
	}

	onDOMCreated(): void {};
}

export interface Container extends Widget
{
	widgets: Widget[];
	addWidget(widget: Widget): void;
	removeWidgetAt(index: number): void;
}

export class VList extends Widget implements Container
{
	static cssClass = "VList";
	static css: string = `.VList ul
{
	list-style: none;
	padding: 0;
	margin: 0;
}
`;
	static html = "<ul></ul>";
	widgets: Widget[] = [];
	addWidget(widget: Widget): void
	{
		this.widgets.push(widget);
		var li = document.createElement("li");
		li.appendChild(widget.domElement);
		li.className = "VList-" + String(this.widgets.length);
		this.domElement.children[0].appendChild(li);
	}
	removeWidgetAt(index: number): void
	{
		var li = this.domElement.getElementsByClassName("VList-" + String(index))[0];
		li.remove();
		this.widgets.splice(index, 1);
	}
	removeWidget(widget: Widget): void
	{
		//TODO: Check if widget is actually in this list
		this.removeWidgetAt(this.widgets.indexOf(widget));
	}
}

export class HList extends VList
{
	static cssClass = "HList";
	static css: string = `.HList ul
{
	list-style: none;
	padding: 0;
	margin: 0;
}

.HList li
{
	display: inline-block;
}`;
}

export class Label extends Widget
{
	static cssClass = "";
	static html = "";
	text: string;

	constructor(text: string)
	{
		super();
		this.text = text;
	}
	
	onDOMCreated(): void
	{
		this.setText(this.text);
	}

	setText(text: string): void
	{
		 this.domElement.innerHTML = text;
	}

	getText(): string
	{
		return this.domElement.innerHTML;
	}
}

export class ImageWidget extends Widget
{
	static html = `<img src="{{= it.src}}"></img>`;
	constructor(src: string)
	{
		super({src: src});
	}
	
	getWidth(): number
	{
		return (<HTMLImageElement> this.domElement.children[0]).width;
	}

	setWidth(width: number): void
	{
		(<HTMLImageElement> this.domElement.children[0]).width = width;
	}
	
	getHeight(): number
	{
		return (<HTMLImageElement> this.domElement.children[0]).height;
	}

	setHeight(height: number): void
	{
		(<HTMLImageElement> this.domElement.children[0]).height = height;
	}
}

export class Input extends Widget
{
	static cssClass = "";
	static html = `<input {{?it.type}}type="{{=it.type}}"{{?}}/>`;
	constructor(type?: string)
	{
		super({type: type});
	}

	getValue(): any
	{
		return this.domElement.getElementsByTagName("input")[0].value;
	}

	setValue(value: any): void
	{
		this.domElement.getElementsByTagName("input")[0].value = value;
	}
}

export class ButtonWidget extends Widget
{
	static cssClass = "";
	static html = "<button>{{= it.content }}</button>"
	constructor(content: string)
	{
		super({content: content});
	}
}

export class SelectWidget extends Widget
{
	static cssClass = "";
	static html = "<select></select>";
	choices: any;

	constructor(choices: any = {})
	{
		super();
		this.choices = choices;
	}
	
	onDOMCreated(): void
	{
		for(var choice in this.choices)
		{
			this.addChoice(choice, this.choices[choice]);
		}
	}

	addChoice(value: string, label: string): void
	{
		var option = document.createElement("option");
		option.value = value;
		option.innerHTML = label;
		this.domElement.getElementsByTagName("select")[0].appendChild(option);
	}

	getValue(): string
	{
		return this.domElement.getElementsByTagName("select")[0].value;
	}

	setValue(value: string): void
	{
		this.domElement.getElementsByTagName("select")[0].value = value;
	}
}

export var language = "en";
var langJSON: any = {
};
export function translate(str: string)
{
	if(langJSON[language] == undefined)
	{
		var xhr = new XMLHttpRequest();
		xhr.open("GET", `lang/${language}.json`, false);
		xhr.send()
		if(xhr.status != 200)
		{
			console.log(`Fetching language file for language ${language} ` +
				`failed: ${xhr.status}`);
			langJSON[language] = null;
		}
		else
		{
			langJSON[language] = JSON.parse(xhr.responseText);
		}
	}
	if(langJSON[language] == null)
	{
		return str;
	}
	else if(langJSON[language][str] == undefined)
	{
		console.log(`Translation for string ${str} in language ${language} ` +
			`not found.`);
		return str;
	}
	else
	{
		return langJSON[language][str];
	}
}

/*document.addEventListener("DOMContentLoaded", function()
{
	init();
	body.setStyle({"margin": "0"});

	var topPanel = new Label("Hello, I'm the top panel!");
	topPanel.setStyle({"position": "fixed", "top": "0px", "width": "100%",
		"height": "40px", "background-color": "#FFDDDD"});
	topPanel.addEventListener("onclick", function(w, e)
	{
		console.log(w);
		console.log(e);
	});
	body.appendChild(topPanel);

	var list = new VList();
	body.appendChild(list);
	list.setStyle({"padding-top": "40px"});
	var label = new Label("Hello world!");
	list.addWidget(label);
	var label2 = new Label("I bees a label!");
	list.addWidget(label2);
	var label3: Label;
	for(var i = 0; i < 50; i++)	
	{
		label3 = new Label(`Label ${i}`);
		list.addWidget(label3);
	}
	label2.setStyle({"background-color": "#FF2299"});
});*/

