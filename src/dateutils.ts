import {translate as t} from "./tswgt";

export function formatDate(date: Date, format: string): string
{
	return format.replace(/%d/, date.getDate().toString())
		.replace(/%m/, (date.getMonth() + 1).toString())
		.replace(/%Y/, date.getFullYear().toString())
		.replace(/%H/, zeroPadStr(date.getHours().toString(), 2))
		.replace(/%M/, zeroPadStr(date.getMinutes().toString(), 2));
}

export function zeroPadStr(str: string, length: number): string
{
	var padding = ""
	for(var i = 0; i < length - str.length; i++)
	{
		padding += "0";
	}

	return padding + str;
}

export class InvalidDateFormatError extends Error
{
	constructor(message: string)
	{
		super(message);
		this.name = "InvalidDateFormatError";
	}
}

export function parseDate(datestr: string, format: string): Date
{
	var date = new Date(0);
	
	var letter: string;
	var numendchar: string;
	var num: number;
	var j = 0; //j - position in datestr, i - position in format
	var i = 0;
	var startj;
	do
	{
		if(i >= format.length)
		{
			break;
		}

		if(format[i] == "%")
		{
			letter = format[++i];
			//I'll assume every format letter represents a number
			num = 0;
			startj = j;
			do
			{
				if(j >= datestr.length || isNaN(parseInt(datestr[j])))
				{
					if(j == startj)
					{
						throw new InvalidDateFormatError("");
					}
					j--;
					break;
				}
				num *= 10;
				num += parseInt(datestr[j]);
			}
			while(++j)
			
			if(letter == "d")
			{
				date.setDate(num);
			}
			else if(letter == "m")
			{
				date.setMonth(num - 1);
			}
			else if(letter == "Y")
			{
				date.setFullYear(num);
			}
			else if(letter == "H")
			{
				date.setHours(num);
			}
			else if(letter == "M")
			{
				date.setMinutes(num);
			}
			
		}
		else if(format[i] != datestr[j])
		{
			throw new InvalidDateFormatError("");
		}
		j++;
	}
	while(i++)

	return date;
}

export class TimeDelta
{
	seconds: number;
	constructor(secs: number = 0, mins: number = 0, hours: number = 0)
	{
		this.seconds = secs + mins * 60 + hours * 3600;
	}
	
	//Substract two dates to get resulting timedelta
	static fromDates(date1: Date, date2: Date)
	{
		return new TimeDelta(date1.getTime() - date2.getTime())
	}

	set minutes(val: number)
	{
		this.seconds += val * 60;
	}

	get minutes()
	{
		return Math.floor(this.seconds / 60);
	}

	set hours(val: number)
	{
		this.seconds += val * 3600;
	}

	get hours()
	{
		return Math.floor(this.seconds / 3600);
	}

	addTo(date: Date)
	{
		date.setTime(date.getTime() + this.seconds);
		return date;
	}

	format(): string
	{
		var format = ((this.hours != 0 ? `%H ${t("timeDelta.hours")} ` : "") +
			((this.minutes - this.hours * 60) != 0 ?
			`%M ${t("timeDelta.minutes")} ` : "") +
			((this.seconds - this.minutes * 60) != 0 ?
			`%S ${t("timeDelta.seconds")}` : "")).trim();
		return format.replace(/%S/, (this.seconds - this.minutes * 60).toString())
			.replace(/%M/, (this.minutes - this.hours * 60).toString())
			.replace(/%H/, this.hours.toString());
	}
}
