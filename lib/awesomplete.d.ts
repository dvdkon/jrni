interface AwesompleteOptions
{
	list?: string[] | HTMLElement | string;
	minChars?: number;
	maxItems?: number;
	autoFirst?: boolean;
	filter?(text: string, input: string): boolean;
	sort?(a: string, b: string): number;
	item?(text: string, input: string): string;
	replace?(text: string): void;
}

declare class Awesomplete
{
	constructor(input: HTMLInputElement, options?: AwesompleteOptions)
	//WARNING: Undocumented API
	input: HTMLInputElement;
	opened: boolean;
	selected: boolean;
	index: number;
	//END WARNING
	list: string[] | HTMLElement | string;
	minChars: number;
	maxItems: number;
	autoFirst: boolean;
	filter(text: string, input: string): boolean;
	sort(a: string, b: string): number;
	item(text: string, input: string): string;
	replace(text: string): void;

	open(): void;
	close(): void;
	next(): void;
	previous(): void;
	goto(i: number): void;
	select(): void;
	evaluate(): void;
}

export = Awesomplete

