#!/usr/bin/env python3
"""
A CGI script to overcome the limitations of cross-domain XMLHttpRequest
It expects to receive one line of data, which it'll append to a predefined
server address and send back the contents of a response from the server.
"""
import os
import sys
import urllib.request
import urllib.error
server_url = "http://main.crws.cz/API.svc"

try:
	inurl = sys.stdin.buffer.read(int(os.environ["CONTENT_LENGTH"])).decode()
except EOFError:
	sys.exit(1)

print("Content-Type: application/json");
print()
try:
	url = urllib.request.urlopen(server_url + inurl)
	print(url.read().decode())
except urllib.error.HTTPError as e:
	print("ERR", e)

